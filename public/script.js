let gsheetdata = [];

getGoogleSheetData();

function getGoogleSheetData(){
	fetch('https://sheets.googleapis.com/v4/spreadsheets/1clgcgnLSYkpz4_Bs9IT3bo1CLCz8_-ZZcPw_mS-P20k/values/BRACKET OQ PLAYOFFS/?alt=json&key=AIzaSyBg7NHk6xeEVsgfcGcM_ql5VoZp42dD8AA')
	.then(function(res) {
		return res.json();
  })
  .then((data) => {
    gsheetdata = data;
	setContent();
  })
}

function setContent() {

  let i = 0;
  
  for (const player of gsheetdata.values) {
	if (i>0) {
		if (player[1]>player[2]) {
			var color1 = "style='color:#f2c933;'"
	        var color2 = "style='color:#FFFFFF;opacity:0.6;'";
			var backcolor1 = "#F2C933";
			var backcolor2 = "rgba(255,255,255,0.6)";
			var colorscore1 = "black";
			var colorscore2 = "white";
		}
		if (player[1]<player[2]) {
			var color1 = "style='color:#FFFFFF;opacity:0.6;'"
	        var color2 = "style='color:#f2c933;'";
			var backcolor2 = "#F2C933";
			var backcolor1 = "rgba(255,255,255,0.6)";
			var colorscore2 = "black";
			var colorscore1 = "white";
		}
		if (player[1]==player[2]) {
			var color1 = "style='color:#FFFFFF;'"
	        var color2 = "style='color:#FFFFFF;'";
			var backcolor1 = "rgba(255,255,255,0)";
			var backcolor2 = "rgba(255,255,255,0)";
		}
		document.querySelector(`.logo${(i*2)-1}`).style.backgroundImage =  `url("./logos/${player[0]}.png")`;
		document.querySelector(`.name${(i*2)-1}`).innerHTML =  `<span ${color1}>${player[0]}</span>`;
		document.querySelector(`.score${(i*2)-1}`).style.backgroundColor = backcolor1;
		document.querySelector(`.score${(i*2)-1}`).innerHTML = `<span style="color:${colorscore1};">${player[1]}</span>`;
		
		document.querySelector(`.logo${i*2}`).style.backgroundImage =  `url("./logos/${player[3]}.png")`;
		document.querySelector(`.name${i*2}`).innerHTML =  `<span ${color2}>${player[3]}</span>`;
		document.querySelector(`.score${i*2}`).style.backgroundColor = backcolor2;
		document.querySelector(`.score${i*2}`).innerHTML = `<span style="color:${colorscore2};">${player[2]}</span>`;

	}	
    i++;
  }
}

function anim(i) {
  return () => {
    document.querySelector(`#team_${i}`).classList.remove('off');
  }
}